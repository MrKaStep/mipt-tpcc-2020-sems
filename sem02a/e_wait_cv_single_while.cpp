#include <iostream>
#include <cassert>
#include <thread>
#include <mutex>
#include <condition_variable>

bool produced;
int produced_value;
std::condition_variable cv;
std::mutex m;

void produce(int value) {
  std::unique_lock<std::mutex> lock(m);

  while (produced)
    cv.wait(lock);

  assert(!produced);
  produced_value = value;
  std::cout << "produced " << value << std::endl;

  produced = true;
  cv.notify_one();
}

void consume() {
  std::unique_lock<std::mutex> lock(m);

  while (!produced)
    cv.wait(lock);

  assert(produced);
  std::cout << "consumed " << produced_value << std::endl;

  produced = false;
  cv.notify_one();
}

static bool mode = false;

int main() {
  std::thread producer([] {
    for (int i = 1; i <= 20; ++i)
      produce(i);
  });

  if (mode) {
    std::thread consumer([] {
      for (int i = 1; i <= 20; ++i)
        consume();
    });
    consumer.join();
  } else {
    std::thread consumer1([] {
      for (int i = 1; i <= 10; ++i)
        consume();
    });
    std::thread consumer2([] {
      for (int i = 1; i <= 10; ++i)
        consume();
    });
    consumer2.join();
    consumer1.join();
  }

  producer.join();
  return 0;
}