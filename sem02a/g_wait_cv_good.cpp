#include <cassert>
#include <iostream>
#include <thread>
#include <mutex>
#include <condition_variable>

bool produced;
int produced_value;
std::condition_variable cv_producer;
std::condition_variable cv_consumer;
std::mutex m;

void produce(int value) {
  std::unique_lock<std::mutex> lock(m);

  while (produced)
    cv_producer.wait(lock);

  assert(!produced);
  produced_value = value;
  std::cout << "produced " << value << std::endl;

  produced = true;
  cv_consumer.notify_one();
}

void consume() {
  std::unique_lock<std::mutex> lock(m);

  while (!produced)
    cv_consumer.wait(lock);

  assert(produced);
  std::cout << "consumed " << produced_value << std::endl;

  produced = false;
  cv_producer.notify_one();
}

int main() {
  std::thread producer1([] {
    for (int i = 1; i < 10; ++i)
      produce(i);
  });
  std::thread producer2([] {
    for (int i = 1; i < 10; ++i)
      produce(i);
  });
  std::thread consumer1([] {
    for (int i = 1; i < 10; ++i)
      consume();
  });
  std::thread consumer2([] {
    for (int i = 1; i < 10; ++i)
      consume();
  });

  producer1.join();
  producer2.join();
  consumer1.join();
  consumer2.join();
  return 0;
}