#include <iostream>
#include <thread>
#include <atomic>

std::atomic<bool> produced;

void produce() {
  std::cout << "produced" << std::endl;
  produced = true;
}

void consume() {
  while (!produced) {
    // noop
  }
  std::cout << "consumed" << std::endl;
}

int main() {
  std::thread producer(produce);
  std::thread consumer(consume);

  producer.join();
  consumer.join();
  return 0;
}