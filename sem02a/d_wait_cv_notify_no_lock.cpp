#include <iostream>
#include <thread>
#include <mutex>
#include <condition_variable>

bool produced = false;
std::condition_variable cv;
std::mutex m;

void produce() {
  std::cout << "produced" << std::endl;
  produced = true;
  cv.notify_one();
}

void consume() {
  std::unique_lock<std::mutex> lock(m);

  while (!produced) {
      cv.wait(lock);
  }

  std::cout << "consumed" << std::endl;
}

int main() {
  std::thread producer(produce);
  std::thread consumer(consume);

  producer.join();
  consumer.join();
  return 0;
}