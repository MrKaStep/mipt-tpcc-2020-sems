#include <iostream>
#include <thread>
#include <mutex>
#include <condition_variable>

// Убрали bool ready
std::condition_variable cv;
std::mutex m;

void produce() {
  std::lock_guard<std::mutex> lock(m);

  std::cout << "produced" << std::endl;

  cv.notify_one();
}

void consume() {
  std::unique_lock<std::mutex> lock(m);

  cv.wait(lock);

  std::cout << "consumed" << std::endl;
}

int main() {
  std::thread producer(produce);
  std::thread consumer(consume);

  producer.join();
  consumer.join();
  return 0;
}