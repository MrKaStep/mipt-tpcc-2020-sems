#include <atomic>
#include <iostream>
#include <mutex>
#include <thread>
#include <vector>

class SpinLock {
public:
    void lock() {
        while (locked_.exchange(true)) {
            // Retry
        }
    }

    void unlock() {
        locked_.store(false);
    }

private:
    std::atomic<bool> locked_{false};
};

void LockGuardExample() {
    SpinLock mutex;
    int guarded_variable = 0;

    auto worker = [&]() {
        std::lock_guard guard(mutex);
        guarded_variable++;
    };

    std::vector<std::thread> workers;
    for (int i = 0; i < 10000; ++i) {
        workers.emplace_back(worker);
    }

    for (auto& w : workers) {
        w.join();
    }

    std::cout << guarded_variable << std::endl;
}

void UniqueLockExample() {
    std::mutex mutex;
    int guarded_variable = 0;

    auto worker = [&]() {
        std::unique_lock lock(mutex);
        guarded_variable++;
        lock.unlock();
        guarded_variable++;
    };

    std::vector<std::thread> workers;
    for (int i = 0; i < 10000; ++i) {
        workers.emplace_back(worker);
    }

    for (auto& w : workers) {
        w.join();
    }

    std::cout << guarded_variable << std::endl;
}

void UniqueLockExample2() {
    std::mutex mutex;
    int guarded_variable = 0;

    auto subworker = [&](std::unique_lock<std::mutex> lock) {
        guarded_variable++;
    };

    auto worker = [&]() {
        std::unique_lock lock(mutex);
        guarded_variable++;
        auto sub = std::thread(subworker, std::move(lock));
        sub.join();
    };

    std::vector<std::thread> workers;
    for (int i = 0; i < 10000; ++i) {
        workers.emplace_back(worker);
    }

    for (auto& w : workers) {
        w.join();
    }

    std::cout << guarded_variable << std::endl;
}

int main() {
    LockGuardExample();
//    UniqueLockExample();
//    UniqueLockExample2();
}
