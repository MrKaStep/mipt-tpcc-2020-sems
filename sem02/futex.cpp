#include <unistd.h>
#include <sys/syscall.h>
#include <linux/futex.h>

#include <atomic>
#include <chrono>
#include <iostream>
#include <limits>
#include <thread>
#include <vector>

using namespace std::chrono_literals;

namespace {

// There is no glibc wrapper for 'futex' syscall

int futex(unsigned int* uaddr, int op, int val, const struct timespec* timeout,
          int* uaddr2, int val3) {
  return syscall(SYS_futex, uaddr, op, val, timeout, uaddr2, val3);
}

}  // namespace


int FutexWait(unsigned int* addr, int expected) {
  return futex(addr, FUTEX_WAIT_PRIVATE, expected, nullptr, nullptr, 0);
}

int FutexWake(unsigned int* addr, int count) {
  return futex(addr, FUTEX_WAKE_PRIVATE, count, nullptr, nullptr, 0);
}

void WakeOneExample() {
    std::atomic<unsigned int> vip_var{0};

    auto worker = [&](int id, int value) {
        std::cerr << "Worker " << id << " waiting on value " << value << std::endl;
        FutexWait(reinterpret_cast<unsigned int*>(&vip_var), value);
        std::cerr << "Worker " << id << " woke up (or never went to sleep)" << std::endl;
    };

    std::vector<std::thread> workers;

    for (int i = 0; i < 3; ++i) {
        workers.emplace_back(worker, i, 0);
    }

    std::this_thread::sleep_for(2s);
    for (int i = 0; i < 3; ++i) {
        FutexWake(reinterpret_cast<unsigned int*>(&vip_var), 1);
        std::this_thread::sleep_for(500ms);
    }

    for (auto& w : workers) {
        w.join();
    }
}

void WakeAllExample() {
    std::atomic<int> vip_var{0};

    auto worker = [&](int id, int value) {
        std::cerr << "Worker " << id << " waiting on value " << value << std::endl;
        FutexWait(reinterpret_cast<unsigned int*>(&vip_var), value);
        std::cerr << "Worker " << id << " woke up (or never went to sleep)" << std::endl;
    };

    std::vector<std::thread> workers;

    for (int i = 0; i < 3; ++i) {
        workers.emplace_back(worker, i, 0);
    }

    std::this_thread::sleep_for(2s);
    FutexWake(reinterpret_cast<unsigned int*>(&vip_var), std::numeric_limits<int>::max());

    for (auto& w : workers) {
        w.join();
    }
}

void NoSleepExample() {
    std::atomic<unsigned int> vip_var{0};

    auto worker = [&](int id, int value) {
        std::cerr << "Worker " << id << " wants to wait on value " << value << std::endl;
        FutexWait(reinterpret_cast<unsigned int*>(&vip_var), value);
        std::cerr << "Worker " << id << " woke up (or never went to sleep)" << std::endl;
    };

    std::vector<std::thread> workers;

    for (int i = 0; i < 3; ++i) {
        workers.emplace_back(worker, i, i);
        std::this_thread::sleep_for(500ms);
    }

    std::this_thread::sleep_for(2s);
    FutexWake(reinterpret_cast<unsigned int*>(&vip_var), 1);

    for (auto& w : workers) {
        w.join();
    }
}

void MultipleQueuesExample() {
    std::atomic<unsigned int> a[2] { {0}, {0} };

    auto worker = [&](int id, unsigned int* addr, int value) {
        std::cerr << "Worker " << id << " waiting on value " << value << " at " << addr << std::endl;
        FutexWait(reinterpret_cast<unsigned int*>(addr), value);
        std::cerr << "Worker " << id << " woke up (or never went to sleep)" << std::endl;
    };

    std::vector<std::thread> workers;

    for (int i = 0; i < 4; ++i) {
        auto addr = reinterpret_cast<unsigned int*>(a + (i % 2));
        workers.emplace_back(worker, i, addr, 0);
        std::this_thread::sleep_for(20ms);
    }

    std::this_thread::sleep_for(2s);
    for (int i = 0; i < 4; ++i) {
        FutexWake(reinterpret_cast<unsigned int*>(a + 1), 2);
        std::this_thread::sleep_for(1s);
    }

    for (auto& w : workers) {
        w.join();
    }
}

int main() {
//    WakeOneExample();
//    WakeAllExample();
//    NoSleepExample();
//    MultipleQueuesExample();
}

