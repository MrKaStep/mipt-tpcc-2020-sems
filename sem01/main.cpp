#include <iostream>
#include <thread>
#include <atomic>
#include <vector>
#include <functional>
#include <chrono>
#include <future>

using namespace std::chrono_literals;

int main()
{
//    std::atomic<int> a = 0;

    auto func = [&](int id) {
        std::cout << "Call #" << id << " " << "Calculating..." << std::endl;
        std::this_thread::sleep_for(2s);
        std::cout << "Call #" << id << " finished" << std::endl;
        return id;
    };

    std::vector<std::future<int>> results;

    for (int i = 0; i < 2; ++i) {
        results.push_back(std::async(std::launch::async, func, i));
    }

    std::cout << "Main sleeping..." << std::endl;
    std::this_thread::sleep_for(500ms);
    std::cout << "Main woke up..." << std::endl;

    for (auto& t : results) {
        std::cout << t.get() << std::endl;
    }

//    std::cout << a << std::endl;

    return 0;
}
